const async = require('../src/async');

const { expect } = require('chai');


describe('Async executor', function() {
  it('should return a promise ', function() {
    const promise = async(function* () {});

    expect(promise).to.be.a('promise');
    expect(async(function* () {}).then).to.be.a('function');
  });

  it('should stop execution and continue after value is resolved', function(done) {
    async(function* () {
      const name = yield getName();
      expect(name).to.be.equal('John Doe');
      done();
    })
  });

  it('should have an ability to use recived arguments', function(done) {
    const name = 'John';

    async(function* (name) {
      expect(name).to.be.equal('John');
      done();
    }, name);
  });

  it('should be fulfilled with returning generator value', function(done) {
    const promise = async(function* () {
      return yield getName();
    });

    promise.then(name => {
      expect(name).to.be.equal('John Doe');
      done();
    });
  });

  it('should be rejected with generator error', function(done) {
    const promise = async(function* () {
      const name = yield getName();
      yield getError();
    });

    promise.catch(err => {
      expect(err).to.be.an('error');
      done();
    })
  });
});

function getError() {
  return new Promise((resolve, reject) => {
    setTimeout(() => { reject(new Error('test error')) }, 100)
  })
}

function getName() {
  return new Promise(resolve => {
    setTimeout(() => { resolve('John Doe') }, 100)
  })
}
