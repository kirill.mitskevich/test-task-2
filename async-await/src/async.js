module.exports = function async(generatorFunc, ...args) {
  return new Promise((resolve, reject) => {
    const generator = generatorFunc(...args);
    executer(generator, resolve, reject);
  })
};


function executer(generator, resolve, reject, ...args) {
  const next = generator.next(...args);

  if (!next.done && next.value.then) {
    next.value.then(
      result => executer(generator, resolve, reject, result)
    ).catch(err => {
      reject(err);
    })
  } else {
    resolve(next.value);
  }
}
