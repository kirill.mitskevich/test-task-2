module.exports = function(generator, ...args) {
  return asyncExecuter(generator(), ...args);
};

function asyncExecuter(generator, ...args) {
  return new Promise((resolve, reject) => {
    // console.log('Args', ...args)
    const next = generator.next(...args);

    if (!next.done && next.value.then) {
      next.value.then(
        result => asyncExecuter(generator, result)
      ).catch(err => {
        reject(err);
      })
    } else {
      console.log('Resolve promise');
      console.log(next)
      resolve(next.value);
    }
  });
}

function ask(question) {
  return new Promise(resolve => {
    setTimeout(function() {
      console.log(question);
      resolve('John Doe');
    }, 1000)
  })
}

function say(text) {
  return new Promise((resolve, reject) => {
    // throw new Error('errrrr');
    setTimeout(function() {
      console.log(text);
      resolve('123');
    }, 1000)
  })
}


function* testGenerator() {
  const name = yield ask('what is your name?');
  yield say(`${name}.`);
  return 'end';
}

const abc = asyncExecuter(testGenerator())
  .then(res => console.log(`Promise fulfill with: ${res}`))
  .catch(err => console.log(`Promise rejected with ${err}`));
