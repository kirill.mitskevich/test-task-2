window.strings = {
  "Please buy {num} oranges": "Пожалуйста, купи {num} апельсин",
  "hello, {name}! I'm from {city}": "Я из {city}... Привет, {name}!"
};

window.addEventListener('load', function() {
  const nodes = document.querySelectorAll('[localize]');

  nodes.forEach(node => {
    const text = node.innerText;
    node.innerText = template(text);
  });
});