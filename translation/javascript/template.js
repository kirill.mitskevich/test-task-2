window.template = function(string) {
  const template = getTemplate(string);

  return template;

  function getTemplate(key) {
    let translatedTemplate = null;

    if (key.match(/{([A-Za-z0-9 _-]+)}/g)) {
      translatedTemplate = getTranslation(key);
    }

    return translatedTemplate || strings[key] || key;
  }

  function getTranslation(key) {
    const initialTemplate = cookTemplate(key);
    const cookedTemplates = Object.keys(window.strings).map(string => cookTemplate(string));
    const translatedTemplate = cookedTemplates.find(template => template.pureText === initialTemplate.pureText);

    if (translatedTemplate) {
      const subs = cookSubs(translatedTemplate.subs, initialTemplate.subs);
      const translation = window.strings[translatedTemplate.initial];

      return compileTemplate(translation, subs);
    }
  }

  function compileTemplate(translation, subs) {
    return translation.replace(/{([A-Za-z0-9 _-]+)}/g, function(match, sub) {
      return typeof subs[sub] !== 'undefined' ?
        subs[sub] : sub;
    })
  }

  function cookSubs(keys, values) {
    return keys.reduce(function(subs, next) {
      subs[next] = values.shift();
      return subs;
    }, {})
  }

  function cookTemplate(template) {
    const subs = [];
    const pureText = template.replace(/{([A-Za-z0-9 _-]+)}/g, function(match, value) {
      subs.push(value);
      return '';
    });

    return {
      initial: template,
      pureText,
      subs
    }
  }
};